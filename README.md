# Scaled Agile Code Interview - Take Home Task

This is a small app called Performance Report built using Lighting Web Components and Apex. Like most apps it is not bug-free, and we'll need to change it from time to time.

## The Task

- The director who uses Performance Report would like to be able to sort the data.

_Bonus points for_

- Fixing any bugs you find along the way. 
- Feel free to make any other changes you'd like, as long as we can easily deploy them through sfdx.

## Time to complete

- Please submit your pull request no later than 5PM MT the day after your interview.

## Steps to complete this task

1. Clone this repo to your computer and deploy it to a Salesforce org of your own (Scratch, Developer, TH Playground, etc.)
    - Note - you may need to [reset your bitbucket password](https://id.atlassian.com/login/resetpassword) to login through git.
    - The easiest way to create a new Trailhead Playground is to [follow this link](https://trailhead.salesforce.com/content/learn/modules/trailhead_playground_management/install-apps-and-packages-in-your-trailhead-playground#challenge).
2. Create a new branch for your work
    - `git checkout -b feature/sort-data`
3. Complete the task, commit your code changes and push to Bitbucket.
    - `git push --set-upstream origin feature/sort-data`
4. Create a pull request to merge your branch into master.

## Notes on this repo

- This repo is in SFDX source format, so you will need to use sfdx to deploy it to Salesforce org.
- It relies on one custom field on the Lead (FakeCreatedDate__c). The rest of the fields are OOTB Salesforce.
- The app queries data from Accounts, Leads and Opportunities. There is a SampleData generator class you can use to create some data.

```java
// Creates 10 accounts, 100 leads and 100 opportunities
SampleData generator = new SampleData();
generator.createData();
```

### Rollup Data

| Column             | Description
| ------             | -----------
| Company            | Name of Account or Company
| Total Leads        | Count of leads for this Company
| Total Opps         | Count of closed won opportunities for this Account
| Conversion Rate    | ratio: # opps / # leads
| Last Closed Date   | Date of last closed won opportunity
| Total Value        | Sum of Amount of all closed won Opportunities

### Screenshot of App

![LWC App](/docs/dev-interview-screenshot1.png)

## Resources

For details on using sfdx, please review the [Salesforce DX Developer Guide](https://developer.salesforce.com/docs/atlas.en-us.sfdx_dev.meta/sfdx_dev).


