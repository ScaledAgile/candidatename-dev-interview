@IsTest
public class TestFactory {
    
    public static User buildUser() {
        return buildUser(new Map<String,Object>{});
    }

    public static User buildUser(String username) {
        String[] nameFields = username.split('@');
        return buildUser(new Map<String,Object>{'Username' => username});
    }
    public static User buildUser(Map<String,Object> fieldOverrides) {
        String DEFAULT_UserName = 'a' + Crypto.getRandomInteger() + '@example.com';
        String DEFAULT_LastName = 'Doba';
        String DEFAULT_FirstName = 'John';
        String DEFAULT_Email = DEFAULT_UserName;
        String DEFAULT_Emailencodingkey = 'UTF-8';
        String DEFAULT_Languagelocalekey = 'en_US';
        String DEFAULT_Localesidkey = 'en_US';
        String DEFAULT_Timezonesidkey = 'America/Denver';
        String DEFAULT_Alias = 'jdoba';

        User newUser = new User(
            Username = DEFAULT_UserName,
            Lastname = DEFAULT_LastName,
            Firstname = DEFAULT_FirstName,
            Email = DEFAULT_Email,
            Emailencodingkey = DEFAULT_Emailencodingkey,
            Languagelocalekey = DEFAULT_Languagelocalekey,
            Localesidkey = DEFAULT_Localesidkey,
            Timezonesidkey = DEFAULT_Timezonesidkey,
            Alias = DEFAULT_Alias,
            Profileid = UserInfo.getProfileId()
        );

        for (String field : fieldOverrides.keyset()) {
            newUser.put(field, fieldOverrides.get(field));
        }
        return newUser;
    }
}
