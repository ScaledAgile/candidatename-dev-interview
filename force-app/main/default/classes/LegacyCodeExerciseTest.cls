@IsTest(IsParallel =false)  //User
class LegacyCodeExerciseTest {
    @TestSetup
    static void makeData() {
        // insert users
        User user1 = TestFactory.buildUser();
        User user2 = TestFactory.buildUser();
        User[] users = new User[]{user1, user2};
        insert users;

        // insert 50 opps
        Opportunity[] opps = new Opportunity[]{};
        for (Integer idx = 0; idx < 50; idx++) {
            Opportunity opp = new Opportunity(
                Name = 'Test Opp ' + idx
                , StageName = 'Closed Won'
                , CloseDate = Date.today().addDays(idx+1)
                , Amount = idx
            );
            opp.OwnerId = users[Math.mod(idx,2)].Id;
            opps.add(opp);
        }
        insert opps;

        // insert 100 leads
        Lead[] leads = new Lead[]{};
        for (Integer idx = 0; idx < 100; idx++) {
            Lead aLead = new Lead(
                LastName = 'LastName ' + idx
                , Company = 'Test, Inc'
                , FakeCreatedDate__c = Datetime.now().addDays(idx+1)
            );
            aLead.OwnerId = users[Math.mod(idx,2)].Id;
            leads.add(aLead);
        }
        insert leads;
    }

    @IsTest
    static void it_should_get_user_rollup_data() {
        LegacyCodeExercise.DataWrapper[] results = LegacyCodeExercise.getUserRollups(Date.today());

        System.assertEquals(2, results.size());
        System.assertNotEquals(null, results[0].ownerId);
        System.assertNotEquals(null, results[0].ownerName);
        System.assertNotEquals(results[0].ownerId, results[1].ownerId);

        for (LegacyCodeExercise.DataWrapper result : results) {
            Integer oppCount = [SELECT Count() FROM Opportunity WHERE OwnerId = :result.ownerId AND StageName = 'Closed Won' AND CloseDate >= TODAY];
            System.assertEquals(oppCount, result.opportunityCount);
            Integer leadCount = [SELECT Count() FROM Lead WHERE OwnerId = :result.ownerId AND FakeCreatedDate__c >= TODAY];
            System.assertEquals(leadCount, result.leadCount);
        }
    }
}