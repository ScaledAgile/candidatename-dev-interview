@Istest(IsParallel=true)
public class PerformanceReportControllerTest {
    
    @TestSetup
    static void makeData(){
        SampleData generator = new SampleData();
        generator.createData();
    }

    @IsTest
    static void it_should_deploy_because_we_have_code_coverage() {
        PerformanceReportController.getPerformanceData(Date.today(), Date.today());
    }
}
