/**
 * Controller for lwc/performanceReport
 * 
 * Gets all leads created in a date range, and all opps closed won in a date range
 * and rolls up the data by Account
 * For leads we will assume the Company field matches the Account Name
 * 
 * Note: for leads we use the FakeCreatedDate__c field in place of CreatedDate for simplicity of the exercise
 */
public without sharing class PerformanceReportController {

    @AuraEnabled(cacheable=true)
    public static PerformanceRecord[] getPerformanceData(Date startDate, Date endDate){
        PerformanceRecord[] results = new PerformanceRecord[]{};

        // fetch data
        // what if dates are null?
        Opportunity[] allOpps = [
            SELECT Id
            , Account.Name
            , CloseDate
            , Amount 
            FROM Opportunity 
            WHERE CloseDate >= :startDate 
            AND CloseDate <= :endDate 
            AND StageName = 'Closed Won'
        ];
        Lead[] allLeads = [
            SELECT Id
            , Company
            FROM Lead 
            WHERE FakeCreatedDate__c >= :startDate 
            AND FakeCreatedDate__c <= :endDate 
        ];

        // group data by account name
        Set<String> accountNames = new Set<String>();
        Map<String, Opportunity[]> oppsByAccountName = new Map<String, Opportunity[]>();
        Map<String, Lead[]> leadsByAccountName = new Map<String, Lead[]>();

        for (Opportunity opp : allOpps) {
            accountNames.add(opp.Account.Name);
            if (oppsByAccountName.containsKey(opp.Account.Name) == false) {
                oppsByAccountName.put(opp.Account.Name, new Opportunity[]{});
            }
            oppsByAccountName.get(opp.Account.Name).add(opp);
        }
        for (Lead aLead : allLeads) {
            accountNames.add(aLead.Company);
            if (leadsByAccountName.containsKey(aLead.Company) == false) {
                leadsByAccountName.put(aLead.Company, new Lead[]{});
            }
            leadsByAccountName.get(aLead.Company).add(aLead);
        }

        // create rollup records
        for (String name : accountNames) {
            Opportunity[] opps = oppsByAccountName.get(name);
            Lead[] leads = leadsByAccountName.get(name);
            if (opps != null || leads != null) {
                PerformanceRecord record = new PerformanceRecord(name, opps, leads);
                results.add(record);
            }
        }

        return results;
    }

    // we use this class for displaying data in a lightning-datatable
    public class PerformanceRecord {
        @AuraEnabled public String accountName {get;set;}
        @AuraEnabled public Integer countLeads {get;set;}
        @AuraEnabled public Integer countOpps {get;set;}
        @AuraEnabled public Decimal conversionRate {get;set;}
        @AuraEnabled public Datetime maxCloseDateOpp {get;set;}
        @AuraEnabled public Decimal totalValueOpp {get;set;}

        public PerformanceRecord(String accountName, Opportunity[] opps, Lead[] leads) {
            this.accountName = accountName;

            // these shouldn't be null, right?
            this.countLeads = leads.size();
            this.countOpps = opps.size();
            this.conversionRate = Decimal.valueOf(this.countOpps) / this.countLeads; 

            Datetime maxCloseDateOpp = Datetime.newInstance(0);
            this.totalValueOpp = 0;
            for (Opportunity opp : opps) {
                this.totalValueOpp += opp.Amount;
                if (maxCloseDateOpp < opp.CloseDate) {
                    maxCloseDateOpp = opp.CloseDate;
                }
            }
            this.maxCloseDateOpp = maxCloseDateOpp;
        }
    }
}
