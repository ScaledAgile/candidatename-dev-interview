/**
 * In this exercise you will be asked to:
 * 1. Review the code
 * 2. Point out parts that will cause an issue (bugs)
 * 3. Point out parts that can be improved (code smells)
 * 4. Use unit tests to refactor the code into something more maintainaable
 */
public with sharing class LegacyCodeExercise {
    /**
     * get a list of users with a count of leads and opportunities for each user
     * opportunities must be closed won
     * filter leads and opps by startDate:
     *      do not include leads created before start date
     *      do not include opps closed before start date
     */
    @AuraEnabled(Cacheable = true)
    public static List<DataWrapper> getUserRollups(Date startDate) {
        String name;
        DataWrapper wrapper;
        Double oppCount = 0.0;
        Double leadCount = 0.0;
        List<DataWrapper> filteredData = new List<DataWrapper>();
        List<DataWrapper> filteredResults = new List<DataWrapper>();
        List<User> users = getUsers();
        List<Lead> leads;
        List<Opportunity> opportunities;

        for (Integer i = 0; i < users.size(); i++) {
            wrapper = new DataWrapper();
            wrapper.OwnerId = users[i].Id;
            wrapper.OwnerName = users[i].Name;
            Set<Id> leadIds = new Set<Id>();
            Set<Id> oppIds = new Set<Id>();

            // count leads and closed won opps owned by this user
            leads = getLeads(users[i].Id);
            opportunities = getOpportunities(users[i].Id);
            for (Lead lead : leads) {
                for (Opportunity opportunity : opportunities) {
                    // if user owns both opp and lead
                    if (users[i].Id == opportunity.OwnerId && users[i].Id == lead.OwnerId) {
                        // filter data by start date
                        Date closeDate = opportunity.CloseDate;
                        Datetime createdDate = lead.FakeCreatedDate__c;
                        if (createdDate > startDate) {
                            if (closeDate > startDate) {
                                // if opp is closed won
                                if (opportunity.StageName == 'Closed Won') {
                                    leadIds.add(lead.Id);
                                    oppIds.add(opportunity.Id);
                                }
                            }
                        }
                    }
                }
            }
            wrapper.opportunityCount = oppIds.size();
            wrapper.leadCount = leadIds.size();

            if (wrapper.opportunityCount > 0 || wrapper.leadCount > 0) {
                filteredData.add(wrapper);
            }
        }

        return filteredData;
    }

    @TestVisible
    private static List<Opportunity> getOpportunities(String ownerId) {
        return [SELECT Id, OwnerId, TotalOpportunityQuantity, CreatedDate,
                Amount, CloseDate, StageName
                FROM Opportunity WHERE OwnerId =: ownerId];
    }

    @TestVisible
    private static List<Lead> getLeads(String ownerId) {
        return
            [SELECT Id, LeadSource, OwnerId, FakeCreatedDate__c, IsConverted FROM Lead
             WHERE OwnerId =: ownerId];
    }


    private static List<User> getUsers() {
        List<User> users = [SELECT Id, Name, Profile.UserLicense.Name FROM User];
        return users;
    }

    public class DataWrapper {
        public String ownerId {get; set;}
        @AuraEnabled public String ownerName { get; set; }
        @AuraEnabled public Double opportunityCount { get; set; }
        @AuraEnabled public Double leadCount { get; set; }
    }
}