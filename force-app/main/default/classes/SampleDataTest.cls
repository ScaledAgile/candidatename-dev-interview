@IsTest(IsParallel=true)
public class SampleDataTest {
    @IsTest
    static void it_should_manage_sample_data() {
        SampleData data = new SampleData(10);
        data.createData();
        System.assertEquals(10, data.accounts.size());
        System.assertEquals(100, data.leads.size());
        System.assertEquals(100, data.opps.size());

        Set<Id> accountIds = new Map<Id, Account>(data.accounts).keyset();
        Set<Id> leadIds = new Map<Id, Lead>(data.leads).keyset();

        // SampleData data = new SampleData(10);
        data.deleteData();
        System.assertEquals(0, [SELECT Id FROM Account WHERE Id IN :accountIds].size());
        System.assertEquals(0, [SELECT Id FROM Lead WHERE Id IN :leadIds].size());
    }
}
