public class SampleData {
    Integer numOrRecords;
    
    @TestVisible
    Account[] accounts;
    @TestVisible
    Lead[] leads;
    @TestVisible
    Opportunity[] opps;

    public SampleData() {
        this.numOrRecords = 10;
    }

    public SampleData(Integer numOrRecords) {
        this.numOrRecords = numOrRecords;
    }

    public void createData() {
        this.accounts = createAccounts();
        this.leads = createLeads(accounts);
        this.opps = createOpportunities(accounts);
    }

    public void deleteData() {
        delete [SELECT Id FROM Lead WHERE Company LIKE 'Test %'];
        delete [SELECT Id FROM Opportunity WHERE Account.Name LIKE 'Test %'];
        delete [SELECT Id FROM Account WHERE Name LIKE 'Test %'];
    }

    private Account[] createAccounts() {
        Account[] newAccounts = new Account[]{};
        for (Integer idx = 0; idx < this.numOrRecords; idx++) {
            Account acct = new Account(
                Name = 'Test ' + idx
            );
            newAccounts.add(acct);
        }
        insert newAccounts;
        return newAccounts;
    }

    private Lead[] createLeads(Account[] newAccounts) {
        Lead[] newLeads = new Lead[]{};
        for (Account acct : newAccounts) {
            String companyName = acct.Name;
            for (Integer idx = 0; idx < this.numOrRecords; idx++) {
                Integer seed1 = Math.mod(Crypto.getRandomInteger(), 2);
                DateTime createdDate = (seed1 == 0 ? Datetime.now().addDays(idx) : Datetime.now().addDays(-idx));
                Lead aLead = new Lead(
                    LastName = 'LastName ' + idx
                    , Company = companyName
                    , FakeCreatedDate__c = createdDate
                );
                newLeads.add(aLead);
            }
        }
        insert newLeads;
        return newLeads;
    }

    private Opportunity[] createOpportunities(Account[] newAccounts) {
        Opportunity[] newOpps = new Opportunity[]{};
        for (Account acct : newAccounts) {
            for (Integer idx = 0; idx < this.numOrRecords; idx++) {
                Integer seed1 = Math.mod(Crypto.getRandomInteger(), 2);
                String stageName = (seed1 == 0 ? 'Closed Won' : 'Closed Lost');
                Integer seed2 = Math.mod(Crypto.getRandomInteger(), 2);
                Date closeDate = (seed2 == 0 ? Date.today().addDays(idx) : Date.today().addDays(-idx));
                Opportunity opp = new Opportunity(
                    Name = 'Test Opp ' + idx
                    , StageName = stageName
                    , CloseDate = closeDate
                    , Amount = Crypto.getRandomInteger()
                    , AccountId = acct.Id
                );
                newOpps.add(opp);   
            }
        }
        insert newOpps;
        return newOpps;
    }
}
