import { LightningElement, track, wire } from 'lwc';
import getPerformanceData from '@salesforce/apex/PerformanceReportController.getPerformanceData';

const MILLIS_IN_DAY = 24*60*60*1000;

export default class PerformanceReport extends LightningElement {

    @track columns;
    @track dataView;

    startDate;
    endDate;

    @wire(getPerformanceData, {startDate: '$startDate', endDate: '$endDate'})
    getPerformanceData({error, data}) {
        this.dataView = data;
    }

    constructor() {
        super();
        this.startDate = new Date(0);
        this.endDate = new Date(new Date().getTime() + 30 * MILLIS_IN_DAY);

        this.columns = [{
            label:'Company',
            fieldName:'accountName'
        },{
            label:'Total Leads',
            fieldName: 'countLeads',
            type: 'number'
        },{
            label: 'Total Opps',
            fieldName: 'countOpps',
            type: 'number'
        },{
            label: 'Conversion Rate',
            fieldName: 'conversionRate',
            type: 'percent'
        },{
            label: 'Last Closed Date',
            fieldName: 'maxCloseDateOpp',
            type: 'date-local'
        },{
            label: 'Total Value',
            fieldName: 'totalValueOpp',
            type: 'currency'
        }];
    }

    handleStartDateChanged(event) {
        this.startDate = new Date(event.target.value);
    }

    handleEndDateChanged(event) {
        this.endDate = new Date(event.target.value);
    }
    
}