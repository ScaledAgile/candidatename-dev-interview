cls/PerformanceReportController

- Leads filtered by end date would ignore leads created after 12AM, need to use datetime and set to one second before midnight
- no null checks on dates in controller, is this ok?

cls/PerformanceReportController.PerformanceRecord

- Opps and Leads lists can be null, no null checks
- countLeads can divide by 0
- max closed date can by 1970 when no opps exist 

lwc/performanceReport

- start date and end date inputs should not be blank
- date input initial values are different from startDate and endDate set in javascript

Other items

- how does it look in mobile?
- how does it perform when we have a lot of data?